<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\JsonApi\V1\StoreProducts\StoreProductRequest;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;
use LaravelJsonApi\Laravel\Http\Controllers\Actions;

class ProductController extends Controller
{

    use Actions\FetchMany;
    use Actions\FetchOne;
    use Actions\Store;
    use Actions\Update;
    use Actions\Destroy;
    use Actions\FetchRelated;
    use Actions\FetchRelationship;
    use Actions\UpdateRelationship;
    use Actions\AttachRelationship;
    use Actions\DetachRelationship;

    public function store(StoreProductRequest $request)
    {
    $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '',$request->data['attributes']['image']));
    $path = Storage::put(time().'_'.$request->data['attributes']['name'].'.png',$image);
        $category = Product::create([
            'name' => $request->data['attributes']['name'],
            'slug' => null,
            'image' => time().'_'.$request->data['attributes']['name'].'.png',
            'category_id' => $request->data['relationships']['category']['data']['id']
        ]);
        return response()->json(['message' => 'success']);
    }
}
