<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\JsonApi\V1\UpdateUsers\UpdateUserRequest;
use App\JsonApi\V1\Users\UserRequest;
use App\Models\User;
use LaravelJsonApi\Laravel\Http\Controllers\Actions;

class UserController extends Controller
{

    use Actions\FetchMany;
    use Actions\FetchOne;
    use Actions\Store;
    use Actions\Update;
    use Actions\Destroy;
    use Actions\FetchRelated;
    use Actions\FetchRelationship;
    use Actions\UpdateRelationship;
    use Actions\AttachRelationship;
    use Actions\DetachRelationship;

    public function store(UserRequest $request)
    {
        User::create($request->data['attributes']);
        return response()->json(['message' => 'success']);
    }
    public function update(UpdateUserRequest $request,User $user)
    {
        $user->update($request->data['attributes']);
        return response()->json(['message' => 'success']);
    }

}
