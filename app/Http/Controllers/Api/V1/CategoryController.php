<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\JsonApi\V1\StoreCategories\StoreCategoryRequest;
use App\JsonApi\V1\StoreCategoryRequests\StoreCategoryRequestRequest;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;
use LaravelJsonApi\Laravel\Http\Controllers\Actions;

class CategoryController extends Controller
{

    use Actions\FetchMany;
    use Actions\FetchOne;
    use Actions\Store;
    use Actions\Update;
    use Actions\Destroy;
    use Actions\FetchRelated;
    use Actions\FetchRelationship;
    use Actions\UpdateRelationship;
    use Actions\AttachRelationship;
    use Actions\DetachRelationship;

    public function store(StoreCategoryRequest $request)
    {
    $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '',$request->data['attributes']['image']));
    $path = Storage::put(time().'_'.$request->data['attributes']['name'].'.png',$image);
        $category = Category::create([
            'name' => $request->data['attributes']['name'],
            'slug' => null,
            'image' => time().'_'.$request->data['attributes']['name'].'.png',
            'user_id' => $request->data['relationships']['owner']['data']['id']
        ]);
        return response()->json(['message' => 'success']);
    }
}
