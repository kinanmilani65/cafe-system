<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::select('*');
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->make(true);
        }

        return view('cafe.index');
    }
    public function create()
    {
        return view('cafe.create');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  $usename
     * @return \Illuminate\Http\Response
     */
    public function edit($username)
    {
        $user = User::where('username',$username)->first();
        return view('cafe.edit',compact('user'));
    }
}
