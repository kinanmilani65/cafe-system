<?php

namespace App\JsonApi\V1;

use App\JsonApi\V1\Categories\CategorySchema;
use App\JsonApi\V1\Products\ProductSchema;
use App\JsonApi\V1\Users\UserSchema;
use App\Models\Category;
use App\Models\User;
use LaravelJsonApi\Core\Server\Server as BaseServer;

class Server extends BaseServer
{

    /**
     * The base URI namespace for this server.
     *
     * @var string
     */
    protected string $baseUri = '/api/v1';

    /**
     * Bootstrap the server when it is handling an HTTP request.
     *
     * @return void
     */
    public function serving(): void
    {

    }

    /**
     * Get the server's list of schemas.
     *
     * @return array
     */
    protected function allSchemas(): array
    {
        return [
            CategorySchema::class,
            ProductSchema::class,
            UserSchema::class,
        ];
    }
}
