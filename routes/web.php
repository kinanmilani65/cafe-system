<?php


use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('',[UserController::class,'index'])->name('index');
Route::get('create',[UserController::class,'create'])->name('create');
Route::get('{username}/edit',[UserController::class,'edit'])->name('edit');
Route::get('{username}',[CategoryController::class,'index'])->name('categories.index');
Route::get('{username}/create',[CategoryController::class,'create'])->name('categories.create');
Route::get('{username}/{slug}',[ProductController::class,'index'])->name('products.index');
Route::get('{username}/{category}/create',[ProductController::class,'create'])->name('products.create');
