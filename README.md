## About the project

It's a cafe's menus management built on Laravel 9.

## How to run

To run this project you have to run the following comands.


``` bash
composer install 
```
then:
``` bash
composer require laravel-json-api/laravel 
composer require --dev laravel-json-api/testing
php artisan vendor:publish --provider="LaravelJsonApi\Laravel\ServiceProvider"
```
``` bash
composer require yajra/laravel-datatable
```

enjoy!



