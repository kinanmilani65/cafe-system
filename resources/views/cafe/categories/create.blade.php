@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="header text-center my-2">
            <h1 class="btn-info">New category</h1>
        </div>
        <div>
            <form id="form" enctype="multipart/form-data">
                <input type="hidden" id="user_id" value="{{ $user->id }}">
                <div class="form-group">
                    <label class=" btn btn-outline-info" for="name">Name</label>
                    <input class="form-control" name="name" id="name">
                </div>
                <div class="form-group">
                    <label class=" btn btn-outline-info" for="name">image</label>
                    <input class="form-control" type="file" name="image" id="image">
                </div>
                <div class="footer">
                    <button type="submit" id="submit">Create</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('#form').on('submit', function(ev) {
            ev.preventDefault();
            let name = $('#name').val();
            let user_id = $('#user_id').val();
            const toBase64 = file => new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = () => resolve(reader.result);
                reader.onerror = error => reject(error);
            });

            async function Main() {
                const file = document.querySelector('#image').files[0];
                let image = await toBase64(file);
                var form_data = {
                    'name': name,
                    'image': image
                }
                $.ajax({
                    url: "{{ url('api/v1/categories') }}",
                    type: "POST",
                    data: JSON.stringify({
                        'data': {
                            'type': 'categories',
                            'attributes': form_data,
                            "relationships": {
                                'owner': {
                                    "data": {
                                        "type": "users",
                                        "id": user_id
                                    }
                                }
                            }
                        }
                    }),
                    headers: {
                        'content-type': 'application/vnd.api+json'
                    },
                    success: function(data, textStatus) {
                        console.log(data)
                        $('#name').val('');
                        Swal.fire({
                            title: 'Success!',
                            text: 'Do you want to continue',
                            icon: 'success',
                            confirmButtonText: 'Continue'
                        })
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        Swal.fire({
                            title: 'Error!',
                            text: 'Sorry, something went wrong, please try again later',
                            icon: 'error',
                            confirmButtonText: 'Continue'
                        });
                    }
                });
            }

            Main();

        })
    </script>
@endsection
