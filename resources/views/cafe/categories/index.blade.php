@extends('layouts.app')
@section('content')


<div class="container">
    <div class="header my-2">
        <a href="/{{$user->username}}/create" class="btn btn-primary w-100">Create new Category</a>
    </div>
    <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Image</th>
                <th width="100px">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax:  "{{ route('categories.index',$user->username) }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'image', name: 'image',
                render: function( data, type, full, meta ) {
                        return "<img src=\"{{Storage::get("+data+")}}\" height=\"50\"/>";
                    }
                },
                {data: 'slug', name: 'slug',
                render: function( data, type, full, meta ) {
                        return "<a href=\"/{{$user->username}}/"+data+"\">products</a>";
                    }
                },
            ]
        });
    });
</script>
@endsection
