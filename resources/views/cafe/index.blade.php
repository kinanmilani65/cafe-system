@extends('layouts.app')
@section('content')


<div class="container">
    <div class="header my-2">
        <a href="/create" class="btn btn-primary w-100">Create new Cafe</a>
    </div>
    <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Username</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
@endsection


@section('scripts')
    <script type="text/javascript">
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax:  "{{ route('index') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'username', name: 'username',
                    render: function( data, type, full, meta ) {
                            return "<a href=\"/"+data+"\">"+data+"</a>";
                        }
                    },
                    {data: 'username', name: 'username',
                    render: function( data, type, full, meta ) {
                            return "<a href=\"/"+data+"/edit\" class=\"btn btn-primary btn-sm\">Edit</a>";
                        }
                    },
                ]
            });
        });
    </script>
@endsection


