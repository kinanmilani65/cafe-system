@extends('layouts.app')
@section('content')
<div class="container">
    <div class="header text-center my-2">
        <h1 class="btn-info">New Cafe registration</h1>
    </div>
    <div>
        <div class="form-group">
            <label class=" btn btn-outline-info" for="name">Name</label>
            <input class="form-control" id="name">
        </div>
        <div class="form-group">
            <label class=" btn btn-outline-info" for="surname">Surname</label>
            <input class="form-control" id="surname">
        </div>
        <div class="form-group">
            <label class=" btn btn-outline-info" for="username">User Name</label>
            <input class="form-control" id="username">
        </div>
        <div class="form-group">
            <label class=" btn btn-outline-info" for="email">Email</label>
            <input class="form-control" type="email" id="email">
        </div>
        <div class="form-group">
            <label class=" btn btn-outline-info" for="password">Password</label>
            <input class="form-control" type="password" id="password">
        </div>
        <div class="footer">
            <button id="submit">Register</button>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('#submit').on('click',function(){
        let name = $('#name').val();
        let surname = $('#surname').val();
        let username = $('#username').val();
        let password = $('#password').val();
        let email = $('#email').val();
        var formData = {
            'name' : name,
            'surname': surname,
            'username':username,
            'password': password,
            'email' : email
        };
        $.ajax({
            url: "{{url('/api/v1/users')}}",
            type: "POST",
            data: JSON.stringify({
                'data':{
                    'type' : 'users',
                    'attributes' : formData
                }
            }),
            headers:{
                'content-type':'application/vnd.api+json'
            },
            success: function(data,textStatus){
                $('#name').val('');
                $('#surname').val('');
                $('#username').val('');
                $('#password').val('');
                $('#email').val('');
                Swal.fire({
                    title: 'Success!',
                    text: 'Do you want to continue',
                    icon: 'success',
                    confirmButtonText: 'Continue'
                })
            },
            error: function (jqXHR, textStatus, errorThrown){
                Swal.fire({
                    title: 'Error!',
                    text: 'Sorry, something went wrong, please try again later',
                    icon: 'error',
                    confirmButtonText: 'Continue'
                });
            }
        });
    })

</script>
@endsection
