@extends('layouts.app')
@section('content')
<div class="container">
    <div class="header text-center my-2">
        <h1 class="btn-info">Editing {{$user->name}} cafe</h1>
    </div>
    <div>
        <div class="form-group">
            <label class=" btn btn-outline-info" for="name">Name</label>
            <input class="form-control" id="name" value="{{$user->name}}">
        </div>
        <div class="form-group">
            <label class=" btn btn-outline-info" for="surname">Surname</label>
            <input class="form-control" id="surname" value="{{$user->surname}}">
        </div>
        <div class="form-group">
            <label class=" btn btn-outline-info" for="username">User Name</label>
            <input class="form-control" id="username" value="{{$user->username}}">
        </div>
        <div class="form-group">
            <label class=" btn btn-outline-info" for="email">Email</label>
            <input class="form-control" type="email" id="email" value="{{$user->email}}">
        </div>
        <div class="footer">
            <button id="submit">Update</button>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('#submit').on('click',function(){
        let name = $('#name').val();
        let surname = $('#surname').val();
        let username = $('#username').val();
        let email = $('#email').val();
        var formData = {
            'name' : name,
            'surname': surname,
            'username':username,
            'email' : email
        };
        $.ajax({
            url: "{{url('/api/v1/users/'.$user->id)}}",
            type: "PATCH",
            data: JSON.stringify({
                'data':{
                    'id': "{{$user->id}}",
                    'type' : 'users',
                    'attributes' : formData
                }
            }),
            headers:{
                'content-type':'application/vnd.api+json'
            },
            success: function(data,textStatus){
                Swal.fire({
                    title: 'Success!',
                    text: 'Cafe\'s updated successfuly',
                    icon: 'success',
                    confirmButtonText: 'Continue'
                })
            },
            error: function (jqXHR, textStatus, errorThrown){
                Swal.fire({
                    title: 'Error!',
                    text: 'Sorry, something went wrong, please try again later',
                    icon: 'error',
                    confirmButtonText: 'Continue'
                });
            }
        });
    })

</script>
@endsection
